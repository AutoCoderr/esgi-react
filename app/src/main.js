import { React } from "./React/React.js";
import { ReactDOM } from "./React/ReactDOM.js";
import { prop_access } from "./React/libs/prop_access.js";
import { interpolate } from "./React/libs/interpolate.js";
import { Index } from "./Components/Index.js";
String.prototype.interpolate = interpolate;
Object.prototype.prop_access = prop_access;

ReactDOM.render(
    React.createElement(Index),
    document.getElementById("root")
);

