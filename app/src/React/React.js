import { type_check, type_check_v1 } from "./libs/type_check.js";

const events = ['click', 'change', 'blur', 'input', 'focus'];

let toFocus = null;

export const React = {
    Component: class {
        props = null;
        rendered = null;
        state = {};
        instancedComponents = {};

        constructor(props) {
            this.props = props
        }

        setState(params) {
            toFocus = null;
            this.state = {...this.state, ...params};
            let newRendered = this.render();
            this.rendered.parentNode.replaceChild(newRendered,this.rendered);
            this.rendered = newRendered;
            if (toFocus != null) {
                toFocus.focus();
                toFocus.setSelectionRange(toFocus.value.length, toFocus.value.length);
            }
        }

        setPath(path) {
            location.hash = "#"+path;
        }

        shouldUpdate = (newProps) => JSON.stringify(this.props) !== JSON.stringify(newProps) || newProps == null;

        display(newProps = null) {
            if (this.shouldUpdate(newProps)) {
                if (newProps != null)
                    this.props = newProps;
                this.rendered = this.render();
            }
            return this.rendered;
        }

        createElement(tagOrComponent, props, children) {
            return React.createElement(tagOrComponent, props, children, this);
        }
    },
    createElement: function(tagOrComponent, props, children = [], parentComponent = null) {
        if (typeof(tagOrComponent) == "string") {
            let element = document.createElement(tagOrComponent);
            for (let child of children) {
                if (type_check_v1(child, "string")) {
                    if (props != null) {
                        child = child.interpolate(props)
                    }
                    let textNode = document.createTextNode(child);
                    element.appendChild(textNode)
                } else {
                    element.appendChild(child);
                }
            }
            for (let attr in props) {
                if (attr !== "prop_access") {
                    if (events.includes(attr))
                        element.addEventListener(attr, props[attr])
                    else
                        element.setAttribute(attr, props[attr]);
                }
            }
            if (document.activeElement.getAttribute("id") != null &&
                document.activeElement.getAttribute("id") === element.getAttribute("id")) {
                toFocus = element
            }
            return element;
        } else {
            if (typeof(tagOrComponent.propType) != "undefined" && !type_check(props,tagOrComponent.propType)) throw TypeError("Vous n'avez pas bien rentrez les props");
            let component;
            let justCreated = false;
            if (parentComponent != null) {
                if (parentComponent.instancedComponents[tagOrComponent.name]) {
                    component = parentComponent.instancedComponents[tagOrComponent.name];
                } else {
                    justCreated = true;
                    component = new tagOrComponent(props);
                    parentComponent.instancedComponents[tagOrComponent.name] = component;
                }
            } else {
                justCreated = true;
                component = new tagOrComponent(props);
            }
            return component.display(!justCreated ? props : null);
        }
    }
}
