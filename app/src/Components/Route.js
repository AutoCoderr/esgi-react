import { React } from "../React/React.js";

export class Route extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {
        return this.createElement("div", null, [
            this.props.currentUri === this.props.uri ?
                this.createElement(this.props.component) :
                ''
        ])
    }
}