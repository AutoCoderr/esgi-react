import { React } from "../React/React.js";

export class Items extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            items: [{name: "toto", content: "Est un margoulin"}],
            name: "",
            content: ""
        };
    }

    addItem = () => {
        const items = this.state.items;
        items.push({name: this.state.name, content: this.state.content})
        this.setState({items, name: "", content: ""});
    }

    onNameInput = (event) => {
        this.setState({
            name: event.target.value
        })
    }

    onContentInput = (event) => {
        this.setState({
            content: event.target.value
        })
    }

    render() {
        return this.createElement("div", null, [
            this.createElement("h1", null, ["Le système d'items :"]),
            this.createElement("table", null, [
                this.createElement("thead", null, [
                    this.createElement("th", null, ["Name"]),
                    this.createElement("th", null, ["Content"])
                ]),
                this.createElement("tbody", null, this.state.items.map((item,index) =>
                    this.createElement("tr", null, [
                        this.createElement("td", null, [item.name]),
                        this.createElement("td", null, [item.content])
                    ])
                ))
            ]),
            this.createElement("h2", null, "Ajouter un élément"),
            this.createElement("label", {for: "name"}, ["Le nom"]),
            this.createElement("input", {id: "name", input: this.onNameInput, value: this.state.name}),
            this.createElement("label", {for: "content"}, ["Le contenu"]),
            this.createElement("input", {id: "content", input: this.onContentInput, value: this.state.content}),
            this.createElement("button", {click: this.addItem}, ["Ajouter"])
        ]);
    }
}