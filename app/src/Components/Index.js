import {React} from "../React/React.js";
import {Home} from "./Home.js";
import {Route} from "./Route.js";
import {Test} from "./Test.js";
import {Nous} from "./Nous.js";
import { Items } from "./Items.js";
import {Counter} from "./Counter.js";
import {Meteo} from "./Meteo.js";

export class Index extends React.Component {
    constructor(props) {
        super(props);
        this.state = {currentUri: location.hash.split("#")[1]}
        window.onhashchange = () => {
            console.log("path changed to " + location.hash.split("#")[1]);
            this.setState({currentUri: location.hash.split("#")[1]})
        }
    }

    render() {
        return this.createElement("div", null, [
            this.createElement("header", null, [
                this.createElement("ul", {style: "background-color: lime; color: #212529; display: flex; justify-content: space-around; list-style: none;"}, [
                    this.createElement("li", {
                        click: () => {
                            this.setPath("/")
                        }
                    }, ["Home"]),
                    this.createElement("li", {
                        click: () => {
                            this.setPath("/test")
                        }
                    }, ["Test"]),
                    this.createElement("li", {
                        click: () => {
                            this.setPath("/nous")
                        }
                    }, ["Nous"]),
                    this.createElement("li", {
                        click: () => {
                            this.setPath("/counter")
                        }
                    }, ["Counter"]),
                    this.createElement("li", {
                        click: () => {
                            this.setPath("/meteo")
                        }
                    }, ["Meteo"]),
                    this.createElement("li", {
                        click: () => {
                            this.setPath("/items")
                        }
                    }, ["Items"])
                ]),
            ]),
            this.createElement(Route, {currentUri: this.state.currentUri, uri: '/', component: Home}),
            this.createElement(Route, {currentUri: this.state.currentUri, uri: '/test', component: Test}),
            this.createElement(Route, {currentUri: this.state.currentUri, uri: '/nous', component: Nous}),
            this.createElement(Route, {currentUri: this.state.currentUri, uri: '/counter', component: Counter}),
            this.createElement(Route, {currentUri: this.state.currentUri, uri: '/meteo', component: Meteo}),
            this.createElement(Route, {currentUri: this.state.currentUri, uri: '/items', component: Items}),
        ]);
    }
}