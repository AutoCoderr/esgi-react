import { React } from "../React/React.js";
import { Image } from "./Image.js";

export class Test extends React.Component {
    constructor(props) {
        super(props);
        const images = ["milky_way.jpeg", "milky_way2.jpeg", "milky_way3.jpeg"];
        this.state = {prenom: "Julien", waiting: true, nbSec: 5, image: images[this.rand(0, images.length-1)]};
        let interval = setInterval(() => {
            const nbSec = this.state.nbSec-1;
            if (nbSec === 0) {
                this.setState({waiting: false, prenom: "Toto", nbSec});
                clearInterval(interval);
            } else {
                this.setState({nbSec});
            }
        }, 1000);
    }

    rand(a,b) {
        return a+Math.floor(Math.random()*(b-a+1));
    }

    render() {
        return this.createElement("section", null, [
            this.createElement("h2", null, ["Le composent parent :"]),
            ( this.state.waiting ?
                this.createElement("div", null, [
                    this.createElement("span", {style: "color: red;"}, "Votre nom va changer dans "+this.state.nbSec+" secondes")
                ]) : ""),
            this.createElement("span", {style: "color: green;", prenom: this.state.prenom}, ["Bonjour {{ prenom }}"]),
            this.createElement("br"),
            this.createElement("h2", null, ["Le sous composant image :"]),
            this.createElement(Image, {image: this.state.image})
            ]
        );
    }
}