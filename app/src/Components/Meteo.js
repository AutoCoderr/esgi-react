import {React} from "../React/React.js";

export class Meteo extends React.Component {

    constructor(props) {
        super(props)
        this.state = {country: "", msg: null}
    }

    onInput = (event) => {
        this.setState({
            country: event.target.value
        })
    }

    callFakeAPI = () => {
        const country = this.state.country
        this.setState({msg: {color:"green", message:"Waiting...."}})
        this.fakeAPI(country)
            .then(response => this.setState({msg:response}))
    }

    rand(a, b) {
        return a + Math.floor(Math.random() * (b - a + 1))
    }

    fakeAPI = (country) => {
        return new Promise(
            resolve => {
                setTimeout(() => {
                    resolve({
                        color: "red",
                        message: "Pranked ! nous n'avons pas d'api meteo pour " + country + ". Enorme BLAGUE !"
                    })
                }, this.rand(2000, 4000))
            }
        )
    }

    render() {

        return this.createElement("div", null, [
            this.createElement("h1", null, ["Pays"]),
            this.createElement("br"),
            this.createElement("input", {type: "text", input: this.onInput, id: "country", value: this.state.country}),
            this.createElement("button", {click: this.callFakeAPI}, ["Valider"]),
            this.createElement("div", null, [
                this.state.msg != null ?
                    this.createElement("span", {style: "color: " + this.state.msg.color, msg: this.state.msg}, [
                        "{{ msg.message }}"
                    ]) : ""
            ])
        ])
    }
}