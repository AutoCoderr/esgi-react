import { React } from "../React/React.js";

export class Counter extends React.Component {
    constructor(props) {
        super(props);
        this.state =  {
            defaultValue: 0
        }
    }

    onInput = (event) => {
        this.setState({
            defaultValue: event.target.value
        })
    }

    onClick = (event) => {
        this.setState({
            defaultValue: parseInt(this.state.defaultValue) + 1
        })
    }

    render() {
        return this.createElement('div', null, [
            this.createElement('input', { type: 'text', id: "counter", input: this.onInput, value: this.state.defaultValue }),
            this.createElement('p', { value: this.state.defaultValue }, [
                "{{ value }}"
            ]),
            this.createElement('button', { click: this.onClick }, [
                "+1"
            ]),
        ])
    }
}