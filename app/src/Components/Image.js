import { React } from "../React/React.js";

export class Image extends React.Component {
    constructor(props) {
        super(props);
        this.state = {waiting: true, nbSec: 10};
        let interval = setInterval(() => {
            const nbSec = this.state.nbSec-1;
            if (nbSec === 0) {
                this.setState({waiting: false, nbSec});
                clearInterval(interval);
            } else {
                this.setState({nbSec});
            }
        }, 1000);
    }


    render() {
        return this.createElement("section", null, [
            ( this.state.waiting ?
                this.createElement("span", {style: "color: red;"}, "Une image va s'afficher dans "+this.state.nbSec+" secondes") :
                this.createElement("img", {src: "/images/"+this.props.image, style: "width: 600px; height: 400px"}))
            ]
        );
    }
}

Image.propType = {
    properties: {
        image: { type: "string" }
    }
}