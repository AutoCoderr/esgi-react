import { React } from "../React/React.js";

export class Nous extends React.Component {

    constructor(props) {
        super(props)
        this.state = {users:[
                {nom: "BOUVET", prenom:"Julien", classe: "4IW1", email: "julienbouvet78@hotmail.com", git: "@AutoCoderr", link: "https://github.com/AutoCoderr"},
                {nom: "CAI", prenom:"Jacques", classe: "4IW1", email: "jacques.cai1999@gmail.com", git: "@JacquesCAI", link: "https://gitlab.com/JacquesCAI"},
                {nom: "CHATEL", prenom:"Thomas", classe: "4IW1", email: "thomas.chatel.acversailles@gmail.com", git: "@thomas.chatel", link:"https://gitlab.com/thomas.chatel"},
            ]}
    }

    render() {
        return this.createElement("div", null, [
            this.createElement("table", null, [
                this.createElement("thead", null, [
                    this.createElement("tr",null,[
                        this.createElement("th",null,["Nom"]),
                        this.createElement("th",null,["Prénom"]),
                        this.createElement("th",null,["Classe"]),
                        this.createElement("th",null,["Email"]),
                        this.createElement("th",null,["Git"]),
                    ])
                ]),
                this.createElement("tbody", null,
                    this.state.users.map((user, index) =>
                        this.createElement("tr", null, [
                            this.createElement("td", null, [user.nom]),
                            this.createElement("td", null, [user.prenom]),
                            this.createElement("td", null, [user.classe]),
                            this.createElement("td", null, [user.email]),
                            this.createElement("td", null, [
                                this.createElement("a", {href: user.link, target: "_blank"}, [user.git]),
                            ]),
                        ])
                    )
                )
            ])
        ])
    }
}